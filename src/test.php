<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Title of the document</title>
    <link rel='stylesheet' id='main-css' href='http://louderwithcrowder.com/main.css?ver=<?php echo rand( 0 , 10000 ); ?>' type='text/css' media='all'/>
</head>

<body style="">

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-561cfdea5bebd9b6" async="async"></script>
<script type="text/javascript">
    var addthis_config = addthis_config||{};
    addthis_config.data_track_addressbar = true;
</script>

<div id="lwc-primary">
    <div class="lwc-ssc-left">
        <div id="lwc-ad-blog-left">
        </div>
    </div>
    <div class="lwc-ssc-right">
    </div>
</div>
<div id="lwc-content">
    <h2 class="lwc-title"></h2>
    <div class="post-content">
        <div class="lwc-post-info">
            <span class="lwc-author"></span>
            <span class="lwc-date"></span>
        </div>
    </div>
    <article class="type-wrap"></article>
    <div id="lwc-ad-single-post-bottom">
    </div>
    <div id="lwc-mobile-post-bottom">
        <div class="addthis_recommended_horizontal"></div>
    </div>
</div>
<div id="lwc-secondary">
    <div class="lwc-subscribe-email">
        <div>
            <i class="fa fa-envelope"></i>
            <span>subscribe to the email</span>
        </div>
        <form method="post" action="https://inboxfirst.maximtech.com/v1/" accept-charset="UTF-8">
            <input type="hidden" value="F8GR4NJKY94T" name="ApiKey">
            <input type="hidden" value="175" name="FormID">
            <input type="hidden" value="http://louderwithcrowder.com/welcome/" name="PassUrl">
            <input type="hidden" value="http://louderwithcrowder.com/failed-validation/" name="FailUrl">
            <input type="text" size="30" name="pending_subscriber[email]" class="subscribe-input">
            <button type="submit" class="subscribe-button">Subscribe</button>
        </form>
    </div>
    <div class="lwc-subscribe-podcast">
        <div>
            <a href="https://soundcloud.com/louderwithcrowder" title="subscribe to podcast" target="_blank" style="color: #ffffff; text-decoration: none;">
                <i class="fa fa-microphone"></i>
                <span>subscribe to the podcast</span>
            </a>
        </div>
    </div>
    <div id="lwc-ad-blog-right">
    </div>
</div>

</body>
 </html>

