<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php wp_title( '' ); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
    <?php wp_head(); ?>
    <script type="text/javascript">
        var width = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;
    </script>
    <script type='text/javascript'>
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];
        (function() {
            var gads = document.createElement('script');
            gads.async = true;
            gads.type = 'text/javascript';
            var useSSL = 'https:' == document.location.protocol;
            gads.src = (useSSL ? 'https:' : 'http:') +
                '//www.googletagservices.com/tag/js/gpt.js';
            var node = document.getElementsByTagName('script')[0];
            node.parentNode.insertBefore(gads, node);
        })();
    </script>
    <script type='text/javascript'>
        googletag.cmd.push(function() {
            googletag.defineSlot('/5298915/LWCBelowArticleTitleDesktop', [[300, 250], [336, 280], [468, 60]], 'div-gpt-ad-1434478860045-0').addService(googletag.pubads());
            googletag.defineSlot('/5298915/LWCSpansTheFoldDesktop', [[300, 250], [300, 600]], 'div-gpt-ad-1434478860045-1').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-61348367-1', 'auto');
        ga('send', 'pageview');

    </script>
   <!-- <script type="text/javascript">window.ayboll=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.ayboll||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="//edge.ayboll.com/ayboll/js/widget.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","ayboll-wjs"));</script>

    <script type="text/javascript">
        if(document.location.protocol=='http:'){
            var Tynt=Tynt||[];Tynt.push('bX5kn417er5iQ1acwqm_6l');
            (function(){var s=document.createElement('script');s.async="async";s.type="text/javascript";s.src='http://tcr.tynt.com/ti.js';var h=document.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);})();
        }
    </script>-->
<script type="text/javascript">
  window._taboola = window._taboola || [];
  _taboola.push({article:'auto'});
  !function (e, f, u, i) {
    if (!document.getElementById(i)){
      e.async = 1;
      e.src = u;
      e.id = i;
      f.parentNode.insertBefore(e, f);
    }
  }(document.createElement('script'),
  document.getElementsByTagName('script')[0],
  '//cdn.taboola.com/libtrc/louderwithcrowder/loader.js',
  'tb_loader_script');
</script>

</head>
<body>
<div id="lwc-container" class="<?php if ( is_front_page() ) { echo 'lwc-home'; } else { echo 'lwc-site'; } ?>">
    <header id="lwc-header">
        <a href="<?php echo site_url(); ?>">
            <img class="lwc-logo" alt="Louder With Crowder" src="<?php echo get_template_directory_uri(); ?>/images/logo.jpg" />
            <img class="lwc-logo-mobile" alt="Louder With Crowder" src="<?php echo get_template_directory_uri(); ?>/images/logo-mobile.jpg" />
        </a>
        <div class="lwc-nav">
            <div class="nav-wrap">
                <nav>
                    <span id="lwc-menu-toggle" class="shiftnav-toggle button" data-shiftnav-target="shiftnav-main"><i class="fa fa-bars"></i></span>
                    <?php wp_nav_menu( array( 'theme_location' => 'top-menu' ) ); ?>
                    <span class="lwc-nav-right">
                        <span class="lwc-social">
                            <a href="https://www.facebook.com/stevencrowderofficial" target="_blank">
                                <i class="fa fa-facebook-square"></i>
                            </a>
                            <a href="https://twitter.com/scrowder" target="_blank">
                                <i class="fa fa-twitter-square"></i>
                            </a>
                            <a href="https://www.youtube.com/user/StevenCrowder/featured" target="_blank">
                                <i class="fa fa-youtube-square"></i>
                            </a>
                        </span>
                        <span class="lwc-search">
                            <i class="fa fa-search"></i>
                        </span>
                    </span>
                </nav>
            </div>
        </div>
        <?php if ( is_active_sidebar( 'lwc_desktop_ad_leaderboard' ) ) : ?>
            <div id="lwc-ad-leaderboard">
                <script type="text/javascript">
                    if ( width < 1000 )
                    {
                        var elem = document.getElementById( 'lwc-ad-leaderboard' );
                        elem.parentNode.removeChild(elem);
                    }
                </script>
                <?php dynamic_sidebar( 'lwc_desktop_ad_leaderboard' ); ?>
            </div>
        <?php endif; ?>
    </header>
    <div id="lwc-wrap">