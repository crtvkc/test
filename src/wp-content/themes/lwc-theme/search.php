<?php get_header(); ?>

    <div id="lwc-primary">
        <div class="lwc-ssc-left">
            <?php if ( is_active_sidebar( 'lwc_blog_left' ) ) : ?>
                <div id="lwc-ad-blog-left">
                    <script type="text/javascript">
                        if ( width < 1000 )
                        {
                            var elem = document.getElementById( 'lwc-ad-blog-left' );
                            elem.parentNode.removeChild(elem);
                        }
                    </script>
                    <?php dynamic_sidebar( 'lwc_blog_left' ); ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="lwc-ssc-right">
            <?php echo do_shortcode( '[social-shares-count-desktop]' ); ?>
        </div>
    </div>
    <div id="lwc-content">
        <?php
        while ( have_posts() ) : the_post(); ?>
            <div class="featured-box">
                <?php
                    $id = get_the_ID();
                    $thumb_id = 0;
                    if ( has_post_thumbnail( $id ) )
                    {
                        $thumb_id = get_post_thumbnail_id($id);
                        $image = wp_get_attachment_image_src($thumb_id, 'blog-large');
                    }
                    if ( empty( $image[0] ) )
                    {
                        $image[0] = get_template_directory_uri() . '/images/thumbnail-660x272.jpg';
                    }
                    $link = get_permalink( $id );
                ?>
                <a href="<?php echo $link; ?>">
                    <img alt="<?php echo lwc_image_alt( $thumb_id ); ?>" class="attachment-blog-large wp-post-image" src="<?php echo $image[0]; ?>">
                </a>
                <h1 class="featured-title">
                    <a href="<?php echo $link; ?>"><?php echo get_the_title(); ?></a>
                </h1>
                <p class="lwc-excerpt"><?php echo get_the_excerpt(); ?></p>
            </div>
        <?php
        endwhile;
        ?>
        <div class="pagination">
            <?php
                global $wp_query;
                $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
                $big = 999999999;

                echo paginate_links( array(
                    'base' => @add_query_arg('paged','%#%'),
                    'format' => '?paged=%#%',
                    'current' => $current,
                    'total' => $wp_query->max_num_pages,
                    'prev_text' => '< Previous',
                    'next_text' => 'Next >'
                ) );
            ?>
        </div>
    </div>
    <div id="lwc-secondary">
        <div class="lwc-subscribe-email">
            <div>
                <i class="fa fa-envelope"></i>
                <span>subscribe to the email</span>
            </div>
            <form method="post" action="https://inboxfirst.maximtech.com/v1/" accept-charset="UTF-8">
                <input type="hidden" value="F8GR4NJKY94T" name="ApiKey">
                <input type="hidden" value="175" name="FormID">
                <input type="hidden" value="http://louderwithcrowder.com/welcome/" name="PassUrl">
                <input type="hidden" value="http://louderwithcrowder.com/failed-validation/" name="FailUrl">
                <input type="text" size="30" name="pending_subscriber[email]" class="subscribe-input">
                <button type="submit" class="subscribe-button">Subscribe</button>
            </form>
        </div>
        <div class="lwc-subscribe-podcast">
            <div>
                <i class="fa fa-microphone"></i>
                <span>subscribe to the podcast</span>
            </div>
        </div>
	<div class="lwc-send-tip">
            <div>
                <a href="mailto:teamcrowder@louderwithcrowder.com?subject=Suggestion%20for%20the%20Crowder%20Show" title="Send A News Article" target="_blank" style="color: #ffffff; text-decoration: none;">
                    <i class="fa fa-paper-plane"></i>
                    <span>send a news article</span>
                </a>
            </div>
        </div>
        <?php if ( is_active_sidebar( 'lwc_blog_right' ) ) : ?>
            <div id="lwc-ad-blog-right">
                <script type="text/javascript">
                    if ( width < 1000 )
                    {
                        var elem = document.getElementById( 'lwc-ad-blog-right' );
                        elem.parentNode.removeChild(elem);
                    }
                </script>
                <?php dynamic_sidebar( 'lwc_blog_right' ); ?>
            </div>
        <?php endif; ?>
    </div>

<?php get_footer(); ?>