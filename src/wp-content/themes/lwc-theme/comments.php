<?php if ( post_password_required() ) { ?>
    <p class="no-comments">This post is password protected. Enter the password to view comments.</p>
<?php return; } ?>

<?php if ( have_comments() ) : ?>
    <div id="comments" class="comments-container">

        <ol class="commentlist">
            <?php wp_list_comments('callback=lwc_comment'); ?>
        </ol>

        <div class="comments-navigation">
            <div class="alignleft"><?php previous_comments_link(); ?></div>
            <div class="alignright"><?php next_comments_link(); ?></div>
        </div>
    </div>
<?php else : ?>
    <?php if ( comments_open() ) : ?>
    <?php else : ?>
        <p class="no-comments">Comments are closed</p>
    <?php endif; ?>
<?php endif; ?>

<?php if ( comments_open() ) : ?>

    <?php

    $comments_args = array(
        'title_reply' => 'Leave A Comment',
        'title_reply_to' => 'Leave A Comment',
        'must_log_in' => '<p class="must-log-in">' .  sprintf( 'You must be %slogged in%s to post a comment.', '<a href="' . wp_login_url( apply_filters( 'the_permalink', get_permalink( ) ) ).'">', '</a>' ) . '</p>',
        'logged_in_as' => '<p class="logged-in-as">Logged in as <a href="' . admin_url( "profile.php" ). '">' . $user_identity . '</a>. <a href="' . wp_logout_url( get_permalink() ). '" title="Log out of this account">Log out &raquo;</a></p>',
        'comment_notes_before' => '',
        'comment_notes_after' => '',
        'comment_field' => '<div id="comment-textarea"><textarea name="comment" id="comment" cols="39" rows="4" tabindex="4" class="textarea-comment" placeholder="Comment..."></textarea></div>',
        'id_submit' => 'comment-submit',
        'class_submit' => 'comment-button',
        'label_submit'=> 'Post Comment',
    );

    comment_form($comments_args);
    ?>

<?php endif;
