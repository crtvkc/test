jQuery(document).ready(function($)
{
    $( '.lwc-search' ).click( function( e ) {
        $( '.lwc-nav-right' ).html( '<form role="search" class="search-form" method="get" action="/"><input type="text" value="" name="s" class="s"><button type="submit"><i class="fa fa-search"></i></button></form>' );
    })

    $( '.lwc-nav' ).sticky({topSpacing:0});
    $( '.lwc-nav' ).on( 'sticky-start', function() { $( this ).addClass( 'lwc-sticky' ); });
    $( '.lwc-nav' ).on( 'sticky-end', function() { $( this ).removeClass( 'lwc-sticky' ); });

    $( '.sticky-widget' ).sticky({topSpacing:50});

    $( 'img.lazy' ).show();
    setTimeout(function(){
        $( 'img.lazy:in-viewport' ).lazyload({
            event: 'lazyload'
        }).trigger( 'lazyload' ).removeClass( 'lazy' );
    }, 10);

    $( window ).scroll(function() {
        $( 'img.lazy:in-viewport' ).lazyload({
            event: 'lazyload'
        }).trigger( 'lazyload' ).removeClass( 'lazy' );
    });

    $(window).resize(function()
    {
        $( 'img.lazy' ).show();
        $( '.sticky-widget' ).unstick();
        $( '.ssc-desktop' ).unstick();
        $( '.sticky-widget' ).sticky({topSpacing:50});
        $( '.ssc-desktop' ).sticky({topSpacing:40});
    });


    //check if on mugclub page
    if(window.location.pathname.match("mugclub") != null)
        return;
    else{
        //check cookie
        var mugclubShown = Cookies.get('mugclubad');

        //if has not been show today, show else do nothing
        if(mugclubShown == 'true')
            return false;
        else{
            setTimeout(showPopup, 15000); // wait 15 secs to show
        }
    }

    function showPopup(){
        
        //modal!
        var popover = $('<div class="white-popup"><a class="mfp-closeme" href="http://crtv.com/mugclub" target="_blank"><img src="/wp-content/themes/lwc-theme/images/crowder_popup.jpg" /></a></div>');
        popover.load(); //so image is preloaded and there isn't a flash of white modal w/ no content
        $.magnificPopup.open({
            items: {
                src: popover, // can be a HTML string, jQuery object, or CSS selector
                type: 'inline'
            },
            closeOnContentClick: true,
            callbacks: {
                open: function(){
                    $.magnificPopup.instance.close = function() {
                        Cookies.set('mugclubad', 'true', {expires: 20*365} ); //expires in 20 years
                        $.magnificPopup.proto.close.call(this);
                    }
                }
            }
        });
        
        Cookies.set('mugclubad', 'true', {expires: 1} );
        return true;
    }




});