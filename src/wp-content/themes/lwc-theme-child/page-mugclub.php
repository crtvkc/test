<?php get_header(); ?>

    <div class="mug-club-wrapper clearfix">
        
            <div class="col lwc-75">
                <div class="videoWrapper">
                    <iframe width="560" height="349" src='https://players.brightcove.net/4988507123001/BkH2ugF1x_default/index.html?videoId=5209788503001' allowfullscreen frameborder=0></iframe>
                </div>
            </div>
            <div class="col lwc-25">
            <?php
                while ( have_posts() ) : the_post(); ?>
                    <div class="type-wrap"><?php the_content(); ?></div>
                    <?php
                endwhile;
            ?>
            </div>
       
    </div>
 
    <div id="blue-bar"></div>  
    
<?php get_footer(); ?>