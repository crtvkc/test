<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php wp_title( '' ); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
    <?php wp_head(); ?>
    <script type="text/javascript">
        var width = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;
    </script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-61348367-1', 'auto');
        ga('send', 'pageview');
    </script>

<?php /*?><!-- START CUSTOM GOOGLE PAGE-LEVEL AD CODE PAN sites -->
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
		google_ad_client: "ca-pub-1613839564205304",
		enable_page_level_ads: true,
		immersives: {google_ad_channel: '6875413670,8352146873'},
		inserts: {google_ad_channel: '6875413670,9828880070'}
    });
</script>
<!-- END CUSTOM GOOGLE PAGE-LEVEL AD CODE --><?php */?>

<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() .'/pan-custom.css?date=20161115"'; ?> media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() .'/pan-custom-media-queries.css?date=20161026"'; ?> media="all" />

<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "22315475" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img src="http://b.scorecardresearch.com/p?c1=2&c2=22315475&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->

<!-- START Quantcast Tag, part 1 of 2 -->
<script type="text/javascript">
    var _qevents = _qevents || [];
    (function() {
        var elem = document.createElement('script');
        elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge")
                    + ".quantserve.com/quant.js";
        elem.async = true;
        elem.type = "text/javascript";
        var scpt = document.getElementsByTagName('script')[0];
        scpt.parentNode.insertBefore(elem, scpt);
    })();
</script>
<!-- END Quantcast Tag, part 1 of 2 -->

</head>
<body>
<div id="lwc-container" class="<?php if ( is_front_page() ) { echo 'lwc-home'; } else { echo 'lwc-site'; } ?>">
    <header id="lwc-header">
        <a href="<?php echo site_url(); ?>">
            <h1><img id="logo" class="lwc-logo" data-mobile-src="<?php echo get_template_directory_uri(); ?>/images/logo-mobile.jpg" data-dsk-src="<?php echo get_template_directory_uri(); ?>/images/logo.jpg" alt="Louder With Crowder" src="<?php echo get_template_directory_uri(); ?>/images/logo.jpg" /></h1>
        </a>
        <div class="lwc-nav">
            <div class="nav-wrap">
                <nav>
                    <span id="lwc-menu-toggle" class="shiftnav-toggle button" data-shiftnav-target="shiftnav-main"><i class="fa fa-bars"></i></span>
                    <?php wp_nav_menu( array( 'theme_location' => 'top-menu' ) ); ?>
                    <span class="lwc-nav-right">
                        <span class="lwc-social">
                            <a href="https://www.facebook.com/stevencrowderofficial" target="_blank" title="Louder with Crowder on Facebook">
                                <i class="fa fa-facebook-square"></i>
                            </a>
                            <a href="https://twitter.com/scrowder" target="_blank" title="Louder with Crowder on Twitter">
                                <i class="fa fa-twitter-square"></i>
                            </a>
                            <a href="https://www.youtube.com/user/StevenCrowder/featured" target="_blank" title="Louder with Crowder on YouTube">
                                <i class="fa fa-youtube-square"></i>
                            </a>
                        </span>
                        <span class="lwc-search">
                            <i class="fa fa-search"></i>
                        </span>
                    </span>
                </nav>
            </div>
        </div>

        <?php get_template_part( 'dsk-banner-ad-a' ); ?>
        <?php get_template_part( 'mob-banner-ad-a' ); ?>
    </header>
    <div id="lwc-wrap">