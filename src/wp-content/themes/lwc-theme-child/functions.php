<?php

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles', 20 );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    //wp_enqueue_script( 'load-post', get_template_directory_uri() . '/js/load-post.js' );
}

/*THIS ALLOWS IMAGES TO BE RESIZED LARGER THAN THE ORIGINAL IMAGES*/

function wpdev_thumbnail_crop_fix( $default, $orig_w, $orig_h, $new_w, $new_h, $crop ){
   if ( !$crop ) return null; // let the wordpress default function handle this

   $aspect_ratio = $orig_w / $orig_h;
   $size_ratio = max($new_w / $orig_w, $new_h / $orig_h);

   $crop_w = round($new_w / $size_ratio);
   $crop_h = round($new_h / $size_ratio);

   $s_x = floor( ($orig_w - $crop_w) / 2 );
   $s_y = floor( ($orig_h - $crop_h) / 2 );

   return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
}
add_filter( 'image_resize_dimensions', 'wpdev_thumbnail_crop_fix', 10, 6 );

/*THIS FORCES THE OG:IMAGE SIZE TO BE AT LEAST 1200x630px*/

add_image_size('facebook', 1200, 630, true);
add_filter( 'wpseo_opengraph_image_size', 'wpdev_wpseo_image_size', 10, 1 );
function wpdev_wpseo_image_size( $string ) {
	return 'facebook';
}
