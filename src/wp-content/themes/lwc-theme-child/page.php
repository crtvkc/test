<?php get_header(); ?>

<?php if ( is_front_page() ) : ?>
    <script type="text/javascript">
        jQuery(document).ready(function($)
        {
            var page = 1;
            var total = <?php $count_posts = wp_count_posts(); echo ceil( $count_posts->publish / 8 ); ?>;
            $( '.load-more' ).on( 'click', function(){
                getPosts( page );
                page++;
            })

            function getPosts( page ){
                $( '.inifinite-load' ).show( 'fast' );
                $.ajax({
                    url: "<?php echo site_url(); ?>/wp-admin/admin-ajax.php",
                    dataType: 'json',
                    type: 'POST',
                    data: "action=infinite_load&page=" + page,
                    success: function( data ){
                        $( '.inifinite-load' ).hide( '1000' );
                        $( '.lwc-recent' ).append( data['recent'] );
                        $( '.lwc-featured' ).append( data['featured'] );
                    }
                });
                return false;
            }
        });
    </script>
    <div id="lwc-primary">
        <?php
            $args = array(
                'numberposts' => 8,
                'offset' => 0,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => 'post',
                'post_status' => 'publish'
            );
            $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
            $count_recent = 0;
        ?>
        <?php if ( count( $recent_posts ) > 0 ) : ?>
            <div class="lwc-recent">
                <h2 class="recent-heading"><span>RECENT POSTS</span></h2>
                <?php $i = 1; ?>
                <?php foreach ( $recent_posts as $recent ) : ?>
                    <?php if ( 2 == $i ) : ?>
                        <?php if ( is_active_sidebar( 'lwc_mobile_home_recent_2nd' ) ) : ?>
                            <div class="recent-box">
                                <div id="lwc-mobile-home-recent-2nd">
                                    <script type="text/javascript">
                                        if ( width > 999 )
                                        {
                                            var elem = document.getElementById( 'lwc-mobile-home-recent-2nd');
                                            elem.parentNode.removeChild(elem);
                                        }
                                    </script>
                                    <?php dynamic_sidebar( 'lwc_mobile_home_recent_2nd' ); ?>

                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if ( 5 == $i ) : ?>
                        <?php if ( is_active_sidebar( 'lwc_mobile_home_recent_5th' ) ) : ?>
                            <div class="recent-box">
                                <div id="lwc-mobile-home-recent-5th">
                                    <script type="text/javascript">
                                        if ( width > 999 )
                                        {
                                            var elem = document.getElementById( 'lwc-mobile-home-recent-5th');
                                            elem.parentNode.removeChild(elem);
                                        }
                                    </script>
                                    <?php dynamic_sidebar( 'lwc_mobile_home_recent_5th' ); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php $i++; ?>
                    <div class="recent-box <?php if ( has_category( '2181', $recent['ID'] ) ) { echo ' recent-featured'; } ?>">
                        <?php
                            ++$count_recent;
                            $thumb_id = 0;
                            if ( has_post_thumbnail( $recent['ID'] ) )
                            {
                                $thumb_id = get_post_thumbnail_id( $recent['ID'] );
                                $image = wp_get_attachment_image_src( $thumb_id, 'blog-medium' );
                            }
                            if ( empty( $image[0] ) )
                            {
                                $image[0] = get_template_directory_uri() . '/images/thumbnail-320x202.jpg';
                            }
                            $link = get_permalink( $recent['ID'] );
                        ?>
                        <a href="<?php echo $link; ?>">
                            <div class="img-container">
                                <img alt="<?php echo lwc_image_alt( $thumb_id ); ?>" class="lazy attachment-blog-medium wp-post-image" src="/wp-content/uploads/2016/10/spacer-320x202.png" data-original="<?php echo $image[0]; ?>" style="display: none;">
                            </div>
                            <noscript><div class="img-container"><img alt="<?php echo lwc_image_alt( $thumb_id ); ?>" class="attachment-blog-medium wp-post-image" src="<?php echo $image[0]; ?>"></div></noscript>
                        </a>
                        <h3 class="recent-title">
                            <a href="<?php echo $link; ?>"><?php echo $recent['post_title']; ?></a>
                        </h3>
                    </div>
                <?php
                if($count_recent == 3){
                    get_template_part( 'ad-contextual-h' );
                }
                ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
    <div id="lwc-content">
        <?php
            $args = array(
                'numberposts' => 6,
                'offset' => 0,
                'category' => 2181,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => 'post',
                'post_status' => 'publish'
            );
            $featured_posts = wp_get_recent_posts( $args, ARRAY_A );
            $count_featured = 0;
        ?>
        <?php if ( count( $featured_posts ) > 0 ) : ?>
            <div class="lwc-featured">
                <h2 class="featured-heading"><span>FEATURED POSTS</span></h2>
                <?php foreach ( $featured_posts as $featured ) : ?>
                    <div class="featured-box">
                        <?php
                            $thumb_id = 0;
                            ++$count_featured;
                            if ( has_post_thumbnail( $featured['ID'] ) )
                            {
                                $thumb_id = get_post_thumbnail_id( $featured['ID'] );
                                $image = wp_get_attachment_image_src( $thumb_id, 'blog-large' );
                            }
                            if ( empty( $image[0] ) )
                            {
                                $image[0] = get_template_directory_uri() . '/images/thumbnail-660x272.jpg';
                            }
                            $link = get_permalink( $featured['ID'] );

                        ?>
                        <a href="<?php echo $link; ?>">
                            <div class="img-container">
                                <img alt="<?php echo lwc_image_alt( $thumb_id ); ?>" class="lazy attachment-blog-large wp-post-image" src="/wp-content/uploads/2016/10/spacer-669x272.png" data-original="<?php echo $image[0]; ?>" style="display: none;">
                            </div>
                            <noscript><img alt="<?php echo lwc_image_alt( $thumb_id ); ?>" class="attachment-blog-large wp-post-image" src="<?php echo $image[0]; ?>"></noscript>
                        </a>
                        <h3 class="featured-title">
                            <a href="<?php echo $link; ?>"><?php echo $featured['post_title']; ?></a>
                        </h3>
                        <p class="lwc-excerpt"><?php echo lwc_excerpt( $featured['ID'], $featured['post_content'] ); ?></p>
                    </div>

                <?php endforeach; ?>
            </div>
            <div class="load-more-container">
              <button class="load-more">Load More</button>
            </div>
        <?php endif; ?>
    </div>
    <div id="lwc-secondary">
        <div class="lwc-subscribe-podcast">
            <div>
                <a href="https://soundcloud.com/louderwithcrowder" title="subscribe to podcast" target="_blank" style="color: #ffffff; text-decoration: none;">
                    <i class="fa fa-microphone"></i>
                    <span>subscribe to the podcast</span>
                </a>
            </div>
        </div>
	<div class="lwc-send-tip">
            <div>
                <a href="mailto:teamcrowder@louderwithcrowder.com?subject=News%20Article%20for%20the%20Crowder%20Show" title="Send a News Article" target="_blank" style="color: #ffffff; text-decoration: none;">
                    <i class="fa fa-paper-plane"></i>
                    <span>send a news article</span>
                </a>
            </div>
        </div>

        <?php get_template_part( 'dsk-box-ad-a' ); ?>
        <?php get_template_part( 'twitter-timeline' ); ?>
        <?php /*?><?php get_template_part( 'ad-contextual-b' ); ?><?php */?>
        <?php get_template_part( 'dsk-box-ad-b' ); ?>

        <?php if ( is_active_sidebar( 'lwc_blog_right' ) ) : ?>
            <div id="lwc-ad-home-right">
                <script type="text/javascript">
                    if ( width < 1000 )
                    {
                        var elem = document.getElementById( 'lwc-ad-home-right');
                        elem.parentNode.removeChild(elem);
                    }
                </script>
                <?php /*?><?php dynamic_sidebar( 'lwc_home_right' ); ?><?php */?>
            </div>
        <?php endif; ?>
    </div>


<?php else : ?>
    <div id="lwc-primary" class="lwc-primary-home">
        <?php if ( is_active_sidebar( 'lwc_blog_left' ) ) : ?>
            <div id="lwc-ad-blog-left">
                <script type="text/javascript">
                    if ( width < 1000 )
                    {
                        var elem = document.getElementById( 'lwc-ad-blog-left');
                        elem.parentNode.removeChild(elem);
                    }
                </script>
                <?php dynamic_sidebar( 'lwc_blog_left' ); ?>
            </div>
        <?php endif; ?>
    </div>
    <div id="lwc-content">
        <?php
            while ( have_posts() ) : the_post(); ?>
                <div class="type-wrap"><?php the_content(); ?></div>
                <?php
            endwhile;
        ?>
    </div>
    <div id="lwc-secondary">
        <?php if ( is_active_sidebar( 'lwc_blog_right' ) ) : ?>
            <div id="lwc-ad-blog-right">
                <script type="text/javascript">
                    if ( width < 1000 )
                    {
                        var elem = document.getElementById( 'lwc-ad-blog-right');
                        elem.parentNode.removeChild(elem);
                    }
                </script>
                <?php dynamic_sidebar( 'lwc_blog_right' ); ?>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>

<?php get_footer(); ?>