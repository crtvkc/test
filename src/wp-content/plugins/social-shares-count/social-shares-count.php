<?php
/*
Plugin Name: Social Shares Count
Plugin URI: http://louderwithcrowder.com/
Description: Social Shares Count for Louder with Crowder.
Version: 0.1
Author: Louder With Crowder
Author URI: http://louderwithcrowder.com
*/
 
class SocialSharesCount 
{
	static function install()
	{
		add_option ( 'lwc_ssc_facebook', 1 );
		add_option ( 'lwc_ssc_googleplus', 1 );
		add_option ( 'lwc_ssc_twitter', 1 );
		add_option ( 'lwc_ssc_pocket', 1 );
		add_option ( 'lwc_ssc_reddit', 1 );
		add_option ( 'lwc_ssc_pinterest', 1 );
		add_option ( 'lwc_ssc_date', 30 );
		add_option ( 'lwc_ssc_counter', 1 );
		wp_schedule_event( time(), 'hourly', 'social_shares_sync' );
	}

	static function uninstall()
	{
		delete_option ( 'lwc_ssc_facebook' );
		delete_option ( 'lwc_ssc_googleplus' );
		delete_option ( 'lwc_ssc_twitter' );
		delete_option ( 'lwc_ssc_pocket' );
		delete_option ( 'lwc_ssc_reddit' );
		delete_option ( 'lwc_ssc_pinterest' );
		delete_option ( 'lwc_ssc_date' );
		delete_option ( 'lwc_ssc_counter' );
		wp_clear_scheduled_hook( 'social_shares_sync' );
	}

	static function addmenu()
	{
		add_submenu_page (
			'options-general.php',
			'Social Count',
			'Social Count',
			'manage_options',
			'social_shares_count',
			array('SocialSharesCount', 'adminHtml')
		);
	}

	static function loadFrontAssets()
	{
        wp_register_script ( 'SocialSharesCountscript', plugins_url( 'social-shares-count/assets/custom.js' ) );
		wp_enqueue_script( 'SocialSharesCountscript' );

		wp_register_style( 'SocialSharesCountstyle', plugins_url( 'social-shares-count/assets/custom.css' ) );
		wp_enqueue_style( 'SocialSharesCountstyle' );

        wp_register_script ( 'SocialSharesCountFixto', plugins_url( 'social-shares-count/assets/jquery.sticky.js' ));
        wp_enqueue_script( 'SocialSharesCountFixto' );
	}

	static function adminHtml()
	{
		if ( !current_user_can( 'manage_options' ) )
		{
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}
		if ( !empty ( $_POST['update'] ) )
		{
			$lwc_ssc_facebook = !empty ( $_POST['lwc_ssc_facebook'] ) ? 1 : 0;
			update_option ( 'lwc_ssc_facebook', $lwc_ssc_facebook );
			$lwc_ssc_googleplus = !empty ( $_POST['lwc_ssc_googleplus'] ) ? 1 : 0;
			update_option ( 'lwc_ssc_googleplus', $lwc_ssc_googleplus );
			$lwc_ssc_twitter = !empty ( $_POST['lwc_ssc_twitter'] ) ? 1 : 0;
			update_option ( 'lwc_ssc_twitter', $lwc_ssc_twitter );
			$lwc_ssc_pocket = !empty ( $_POST['lwc_ssc_pocket'] ) ? 1 : 0;
			update_option ( 'lwc_ssc_pocket', $lwc_ssc_pocket );
			$lwc_ssc_reddit = !empty ( $_POST['lwc_ssc_reddit'] ) ? 1 : 0;
			update_option ( 'lwc_ssc_reddit', $lwc_ssc_reddit );
			$lwc_ssc_pinterest = !empty ( $_POST['lwc_ssc_pinterest'] ) ? 1 : 0;
			update_option ( 'lwc_ssc_pinterest', $lwc_ssc_pinterest );
			$lwc_ssc_date = !empty ( $_POST['lwc_ssc_date'] ) ? ceil ( $_POST['lwc_ssc_date'] ) : 0;
			update_option ( 'lwc_ssc_date', $lwc_ssc_date );

			$updated = 1;
		}
	?>
		<div class="crm-wrap">
			<h2>Social Plugin Settings</h2>
			<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST">
				<input type="hidden" value="1" name="update" />
				<ul>
					<br>
					<li><input type="checkbox" value="1" name="lwc_ssc_facebook" <?php if ( 1 == get_option ( 'lwc_ssc_facebook' ) ) { echo 'checked'; } ?> /> Facebook</li>
					<li><input type="checkbox" value="1" name="lwc_ssc_googleplus" <?php if ( 1 == get_option ( 'lwc_ssc_googleplus' ) ) { echo 'checked'; } ?> /> Google+</li>
					<li><input type="checkbox" value="1" name="lwc_ssc_twitter" <?php if ( 1 == get_option ( 'lwc_ssc_twitter' ) ) { echo 'checked'; } ?> /> Twitter</li>
					<li><input type="checkbox" value="1" name="lwc_ssc_pocket" <?php if ( 1 == get_option ( 'lwc_ssc_pocket' ) ) { echo 'checked'; } ?> /> Pocket</li>
					<li><input type="checkbox" value="1" name="lwc_ssc_reddit" <?php if ( 1 == get_option ( 'lwc_ssc_reddit' ) ) { echo 'checked'; } ?> /> Reddit</li>
					<li><input type="checkbox" value="1" name="lwc_ssc_pinterest" <?php if ( 1 == get_option ( 'lwc_ssc_pinterest' ) ) { echo 'checked'; } ?> /> Pinterest</li>
					<li>Sync posts published before: <input type="text" value="<?php echo get_option ( 'lwc_ssc_date' ); ?>" name="lwc_ssc_date" /> days</li>
					<br><br>
				</ul>
	   			<input type="submit" class="button-primary" value="Save Settings">
			</form>
			<?php if ( !empty ( $updated ) ): ?>
   				<p>Settings were updated successfully!</p>
   			<?php endif; ?>
		</div>
	<?php
	}

	static function frontDesktopHtml()
    {
        global $wp_query;
        global $wpdb;

        $postid = $wp_query->get_queried_object_id();
        if (is_single($postid))
        {
            $title = html_entity_decode( get_the_title(), ENT_COMPAT, 'UTF-8' );
            $permalink = get_permalink($postid);

            if (has_post_thumbnail($postid)) {
                $postimgs = wp_get_attachment_image_src(get_post_thumbnail_id($postid), 'large');
                $postimg = $postimgs[0];
            } else {
                $postimg = plugins_url('social-shares-count/assets/images/louderwithcrowder.jpg');
            }

            $socialmeta = get_post_meta($postid);

            $ssc_count_facebook = !empty ($socialmeta['ssc_count_facebook'][0]) ? $socialmeta['ssc_count_facebook'][0] : 0;
            $ssc_count_googleplus = !empty ($socialmeta['ssc_count_googleplus'][0]) ? $socialmeta['ssc_count_googleplus'][0] : 0;
            $ssc_count_twitter = !empty ($socialmeta['ssc_count_twitter'][0]) ? $socialmeta['ssc_count_twitter'][0] : 0;
            $ssc_count_reddit = !empty ($socialmeta['ssc_count_reddit'][0]) ? $socialmeta['ssc_count_reddit'][0] : 0;
            $ssc_count_pinterest = !empty ($socialmeta['ssc_count_pinterest'][0]) ? $socialmeta['ssc_count_pinterest'][0] : 0;

            if (!empty ($socialmeta['_yourls_url'][0])) {
                $permalink = $socialmeta['_yourls_url'][0];
            }

            $ssc_total = 0;

            if (!empty ($ssc_count_facebook)) {
                $ssc_total += $ssc_count_facebook;
            }
            if (!empty ($ssc_count_googleplus)) {
                $ssc_total += $ssc_count_googleplus;
            }
            if (!empty ($ssc_count_twitter)) {
                $ssc_total += $ssc_count_twitter;
            }
            if (!empty ($ssc_count_reddit)) {
                $ssc_total += $ssc_count_reddit;
            }
            if (!empty ($ssc_count_pinterest)) {
                $ssc_total += $ssc_count_pinterest;
            }

            $fbon = get_option('lwc_ssc_facebook');
            $gpluson = get_option('lwc_ssc_googleplus');
            $twon = get_option('lwc_ssc_twitter');
            $pkon = get_option('lwc_ssc_pocket');
            $redon = get_option('lwc_ssc_reddit');
            $pinon = get_option('lwc_ssc_pinterest');

            $custom = get_post_custom( $postid );
            if( ! empty( $custom['_yoast_wpseo_twitter-title'][0] ) )
            {
                $twtitle = $custom['_yoast_wpseo_twitter-title'][0];
            }
            else {
                $twtitle = $title;
            }

            ?>
            <div class="ssc-desktop">
                <?php if($ssc_total >= 3000) : ?>
                    <p class="ssc-total"><?php echo $ssc_total; ?></p>
                    <p class="ssc-text">SHARES</p>
                    <hr>
               <?php endif; ?>
                <?php if ( ! empty( $fbon ) ) :  ?>
                    <a class="ssc-bubble" target="_blank" href="<?php echo 'https://www.facebook.com/sharer/sharer.php?'.http_build_query(array('u' => $permalink)); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=285,width=600'); return false;">
                        <img src="<?php echo plugins_url( 'social-shares-count/assets/images/facebook.png' ); ?>" alt="facebook icon" />
                        <span>FACEBOOK</span>
                    </a>
                    <?php if ( !empty ( $ssc_count_facebook ) ) : ?>
                        <span class="count-bubble"><?php echo $ssc_count_facebook; ?></span>
                    <?php endif; ?>
                    <hr>
                <?php endif; ?>
                <?php if ( ! empty( $twon ) ) :  ?>
                    <a class="ssc-bubble"  target="_blank" href="<?php echo 'https://twitter.com/intent/tweet?'.http_build_query(array('text' => $twtitle.' '.$permalink.'','via' => 'scrowder')); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=220,width=600'); return false;">
                        <img src="<?php echo plugins_url( 'social-shares-count/assets/images/twitter.png' ); ?>" alt="twitter icon" />
                        <span>TWITTER</span>
                    </a>
                    <?php if ( !empty ( $ssc_count_twitter ) ) : ?>
                        <span class="count-bubble"><?php echo $ssc_count_twitter; ?></span>
                    <?php endif; ?>
                    <hr>
                <?php endif; ?>
                <a target="_blank" href="<?php echo 'mailto:?subject=' . $title . '&body=' . $permalink; ?>">
                    <img src="<?php echo plugins_url( 'social-shares-count/assets/images/email.png' ); ?>" alt="email icon" />
                    <span>EMAIL</span>
                </a>
                <hr>
                <div class="more-bubble">
                    <img src="<?php echo plugins_url( 'social-shares-count/assets/images/more.png' ); ?>" alt="more icon" />
                    <span>MORE</span>
                    <div class="wrap-bubble">
                        <img class="bubble-triangle" src="<?php echo plugins_url( 'social-shares-count/assets/images/triangle.png' ); ?>" />
                        <?php if ( ! empty( $fbon ) ) : ?>
                            <a target="_blank"
                               href="<?php echo 'https://www.facebook.com/sharer/sharer.php?' . http_build_query(array('u' => $permalink)); ?>"
                               onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=285,width=600'); return false;">
                                <img class="ssc-icon"
                                     src="<?php echo plugins_url('social-shares-count/assets/images/hover/facebook.png'); ?>"
                                     alt="facebook"/>
                                <?php if (!empty ($ssc_count_facebook)) : ?>
                                    <span class="count-v-bubble"><?php echo $ssc_count_facebook; ?></span>
                                <?php endif; ?>
                            </a>
                        <?php endif; ?>
                        <?php if ( ! empty( $gpluson ) ) : ?>
                            <a target="_blank" href="<?php echo 'https://plus.google.com/share?url=' . $permalink; ?>"
                               onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;">
                                <img class="ssc-icon"
                                     src="<?php echo plugins_url('social-shares-count/assets/images/hover/googleplus.png'); ?>"
                                     alt="google plus"/>
                                <?php if (!empty ($ssc_count_googleplus)) : ?>
                                    <span class="count-v-bubble"><?php echo $ssc_count_googleplus; ?></span>
                                <?php endif; ?>
                            </a>
                        <?php endif; ?>
                        <?php if ( ! empty( $twon ) ) : ?>
                            <a target="_blank"
                               href="<?php echo 'https://twitter.com/intent/tweet?' . http_build_query(array('text' => $twtitle . ' ' . $permalink . '', 'via' => 'scrowder')); ?>"
                               onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=220,width=600'); return false;">
                                <img class="ssc-icon"
                                     src="<?php echo plugins_url('social-shares-count/assets/images/hover/twitter.png'); ?>"
                                     alt="twitter"/>
                                <?php if (!empty ($ssc_count_twitter)) : ?>
                                    <span class="count-v-bubble"><?php echo $ssc_count_twitter; ?></span>
                                <?php endif; ?>
                            </a>
                        <?php endif; ?>
                        <?php if ( ! empty( $pkon ) ) : ?>
                            <a target="_blank"
                               href="<?php echo 'https://getpocket.com/save?' . http_build_query(array('url' => $permalink, 'title' => $title)); ?>"
                               onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600'); return false;">
                                <img src="<?php echo plugins_url('social-shares-count/assets/images/hover/pocket.png'); ?>"
                                     alt="pocket"/>
                            </a>
                        <?php endif; ?>
                        <?php if ( ! empty( $redon ) ) : ?>
                            <a target="_blank"
                               href="<?php echo 'http://www.reddit.com/submit?' . http_build_query(array('url' => $permalink, 'title' => $title)); ?>">
                                <img class="ssc-icon"
                                     src="<?php echo plugins_url('social-shares-count/assets/images/hover/reddit.png'); ?>"
                                     alt="reddit"/>
                                <?php if (!empty ($ssc_count_reddit)) : ?>
                                    <span class="count-v-bubble"><?php echo $ssc_count_reddit; ?></span>
                                <?php endif; ?>
                            </a>
                        <?php endif; ?>
                        <?php if ( ! empty( $pinon ) ) : ?>
                            <a target="_blank"
                               href="<?php echo 'http://pinterest.com/pin/create/button/?' . http_build_query(array('url' => $permalink, 'description' => $title, 'media' => $postimg)); ?>">
                                <img class="ssc-icon"
                                     src="<?php echo plugins_url('social-shares-count/assets/images/hover/pinterest.png'); ?>"
                                     alt="pinterest"/>
                                <?php if (!empty ($ssc_count_pinterest)) : ?>
                                    <span class="count-v-bubble"><?php echo $ssc_count_pinterest; ?></span>
                                <?php endif; ?>
                            </a>
                        <?php endif; ?>
                        <a target="_blank" href="<?php echo 'mailto:?subject=' . $title . '&body=' . $permalink; ?>">
                            <img src="<?php echo plugins_url('social-shares-count/assets/images/hover/email.png'); ?>"
                                 alt="email"/>
                        </a>
                        <a data-yourls="<?php echo $permalink; ?>" class="copy-yourls">
                            <img src="<?php echo plugins_url('social-shares-count/assets/images/hover/permalink.png'); ?>"
                                 alt="permalink"/>
                        </a>
                    </div>
                </div>
            </div>
            <?php
        }
    }

	static function social_sync_count()
	{
		global $wpdb;
		set_time_limit( 3600 );

		$lwc_ssc_counter = get_option ( 'lwc_ssc_counter' );
		if ( $lwc_ssc_counter > 23 )
		{
			update_option ( 'lwc_ssc_counter', 1 );
			$posts = $wpdb->get_results( "SELECT ID FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post'");
		}
		else
		{
			$lwc_ssc_counter++;
			update_option ( 'lwc_ssc_counter', $lwc_ssc_counter );
			$olddate = '-' . get_option ( 'lwc_ssc_date' ) . ' days';
			$querydate = date('Y-m-d', strtotime( $olddate ) );
			$posts = $wpdb->get_results( "SELECT ID FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' AND post_date > '$querydate'");
		}

		foreach ( $posts as $post )
		{
	        $permalink = get_permalink( $post->ID );
	        $response	= wp_remote_get( "http://free.sharedcount.com/?url=" . rawurlencode( $permalink ) . "&apikey=44cbbca9c41bd48787f5d9ffd4798262507ee8e7" );
	        if( is_wp_error( $response ) )
	        	return;
	        $counts = json_decode( $response['body'], true );

	        if ( 1 == get_option ( 'lwc_ssc_facebook' ) && isset ( $counts['Facebook']['total_count'] ) && $counts['Facebook']['total_count'] > 0 )
	        {
	        	update_post_meta( $post->ID, 'ssc_count_facebook', $counts['Facebook']['total_count'] );
	        }
	        if ( 1 == get_option ( 'lwc_ssc_googleplus' ) && isset ( $counts['GooglePlusOne'] ) && $counts['GooglePlusOne'] > 0 )
	        {
	        	update_post_meta( $post->ID, 'ssc_count_googleplus', $counts['GooglePlusOne'] );
	        }
	        if ( 1 == get_option ( 'lwc_ssc_twitter' ) && isset ( $counts['Twitter'] ) && $counts['Twitter'] > 0 )
	        {
	        	update_post_meta( $post->ID, 'ssc_count_twitter', $counts['Twitter'] );
	        }
	        if ( 1 == get_option ( 'lwc_ssc_pinterest' ) && isset ( $counts['Pinterest'] ) && $counts['Pinterest'] > 0 )
	        {
	        	update_post_meta( $post->ID, 'ssc_count_pinterest', $counts['Pinterest'] );
	        }

	        unset ( $response );
	        unset ( $counts );

	        if ( 1 == get_option ( 'lwc_ssc_reddit' ) )
	        {
	        	$response	= wp_remote_get( "http://www.reddit.com/api/info.json?url=" . rawurlencode( $permalink ) );
	        	if( is_wp_error( $response ) )
	        		return;
	        	$counts = json_decode( $response['body'], true );
	        	
	        	if ( isset ( $counts['data']['children'][0]['data']['num_comments'] ) && $counts['data']['children'][0]['data']['num_comments'] >= 0 )
	        	{
	        		update_post_meta( $post->ID, 'ssc_count_reddit', $counts['data']['children'][0]['data']['num_comments'] );
	        	}
	        	
	        	unset ( $response );
	        	unset ( $counts );
	        }
	    }
	}
}

$sync_cron = wp_get_schedule( 'social_shares_sync' );
if ( empty ( $sync_cron ) )
{
    wp_schedule_event( time(), 'hourly', 'social_shares_sync' );
}

if ( is_admin() )
{
	register_activation_hook( __FILE__, array( 'SocialSharesCount', 'install' ) );
	register_deactivation_hook( __FILE__, array( 'SocialSharesCount', 'uninstall' ) );
	add_action( 'admin_menu', array( 'SocialSharesCount', 'addmenu' ) );
}
else
{
	add_action( 'wp_enqueue_scripts', array( 'SocialSharesCount', 'loadFrontAssets' ) );
	add_shortcode ('social-shares-count-desktop', array( 'SocialSharesCount', 'frontDesktopHtml' ) );
}

add_action( 'social_shares_sync', array( 'SocialSharesCount', 'social_sync_count' ) );