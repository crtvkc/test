jQuery(document).ready(function($)
{
    jQuery('.ssc-desktop').sticky({topSpacing:40});

    jQuery(document).on('click', '.copy-yourls', function() {
        var yourls = jQuery(this).attr('data-yourls');
        window.prompt("Copy to clipboard: Ctrl+C, Enter", yourls);
    });

    jQuery(document).on('click', '.more-bubble', function() {
        jQuery(this).find('.wrap-bubble').toggle();
    });

    jQuery.fn.center = function ()
    {
        this.css("position","fixed");
        this.css("top", (jQuery(window).height() / 2) - (this.outerHeight() / 2));
        this.css("left", (jQuery(window).width() / 2) - (this.outerWidth() / 2));
        return this;
    }

});