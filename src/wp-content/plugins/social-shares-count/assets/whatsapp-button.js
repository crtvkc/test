waShBtn = function() {
    this.isIos === !0 && this.cntLdd(window, this.crBtn)
}, waShBtn.prototype.isIos = navigator.userAgent.match(/Android|iPhone/i) && !navigator.userAgent.match(/iPod|iPad/i) ? !0 : !1, waShBtn.prototype.cntLdd = function(win, fn) {
    var done = !1,
        top = !0,
        doc = win.document,
        root = doc.documentElement,
        add = doc.addEventListener ? "addEventListener" : "attachEvent",
        rem = doc.addEventListener ? "removeEventListener" : "detachEvent",
        pre = doc.addEventListener ? "" : "on",
        init = function(e) {
            ("readystatechange" != e.type || "complete" == doc.readyState) && (("load" == e.type ? win : doc)[rem](pre + e.type, init, !1), !done && (done = !0) && fn.call(win, e.type || e))
        },
        poll = function() {
            try {
                root.doScroll("left")
            } catch (e) {
                return void setTimeout(poll, 50)
            }
            init("poll")
        };
    if ("complete" == doc.readyState) fn.call(win, "lazy");
    else {
        if (doc.createEventObject && root.doScroll) {
            try {
                top = !win.frameElement
            } catch (e) {}
            top && poll()
        }
        doc[add](pre + "DOMContentLoaded", init, !1), doc[add](pre + "readystatechange", init, !1), win[add](pre + "load", init, !1)
    }
}, waShBtn.prototype.addStyling = function() {
    var s = document.createElement("style"),
        c = "body,html{padding:0;margin:0;height:100%;width:100%}.wa_btn{display:inline-block!important;position:relative;font-family:Arial,sans-serif;letter-spacing:.4px;cursor:pointer;font-weight:400;text-transform:none;color:#fff;border-radius:2px;background-color:#5cbe4a;background-repeat:no-repeat;line-height:1.2;text-decoration:none;text-align:left}.wa_btn_s{font-size:12px;background-size:16px;background-position:5px 2px;padding:3px 6px 3px 25px}.wa_btn_m{font-size:16px;background-size:20px;background-position:4px 2px;padding:4px 6px 4px 30px}.wa_btn_l{font-size:16px;background-size:20px;background-position:5px 5px;}";
    return s.type = "text/css", s.styleSheet ? s.styleSheet.cssText = c : s.appendChild(document.createTextNode(c)), s
}, waShBtn.prototype.crBtn = function() {
    var b = [].slice.call(document.querySelectorAll(".wa_btn"));
    iframe = new Array;
    for (var i = 0; i < b.length; i++) {
        var parent = b[i].parentNode,
            t = b[i].getAttribute("data-text"),
            u = b[i].getAttribute("data-href"),
            o = b[i].getAttribute("href"),
            at = "?text=" + encodeURIComponent(t);
        t && (at += "%20"), at += encodeURIComponent(u ? u : document.URL), b[i].setAttribute("href", o + at), b[i].setAttribute("target", "_top"), iframe[i] = document.createElement("iframe"), iframe[i].width = 1, iframe[i].height = 1, iframe[i].button = b[i], iframe[i].style.border = 0, iframe[i].style.overflow = "hidden", iframe[i].border = 0, iframe[i].setAttribute("scrolling", "no"), iframe[i].addEventListener("load", function() {
            this.contentDocument.body.appendChild(this.button), this.contentDocument.getElementsByTagName("head")[0].appendChild(theWaShBtn.addStyling());
            var meta = document.createElement("meta");
            meta.setAttribute("charset", "utf-8"), this.contentDocument.getElementsByTagName("head")[0].appendChild(meta), this.width = Math.ceil(this.contentDocument.getElementsByTagName("a")[0].getBoundingClientRect().width), this.height = Math.ceil(this.contentDocument.getElementsByTagName("a")[0].getBoundingClientRect().height)
        }, !1), parent.insertBefore(iframe[i], b[i])
    }
};
var theWaShBtn = new waShBtn;