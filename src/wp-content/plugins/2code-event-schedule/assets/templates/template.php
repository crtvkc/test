<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 27.01.16
 * Time: 23:20
 */

global $post;

$dateCount = count($postArray);
$dateCols = apply_filters('2code-count-columns', $dateCount);

$maxSM = 3;
$maxXSDate = 4;
$maxXSLocation = 3;

$cnt = count($postArray);

$dateFormat = get_option('2code_es_general_date_format', 'D • d/m/Y');
$timeFormat = get_option('2code_es_general_time_format', 'h:i A');
?>

<div class="container-fluid event-schedule tcode-event-schedule">
    <div class="col-xs-12">
        <div class="row scheduled-days">
            <?php

            $mdColSize = $dateCols;

            $colSize = $mdColSize;
            $cols = 12/$mdColSize;

            ?>
            <div class="carousel hidden-mobile" id="days-carousel">
                <?php for($i=0;$i<$dateCount;$i++): ?>
                    <?php

                    $current = $postArray[$i];
                    $day = $current['day'];

                    ?>
                    <?php $date = str_replace('/', '-', get_field('event_day_date', $day->ID)); ?>
                    <?php $d = new DateTime($date) ?>
                    <div class="slick-slide scheduled-day" data-date="<?= $d->format('Y-m-d') ?>">
                        <div class="row">
                            <div class="col-xs-12 row-day"><?= get_the_title($day->ID); ?></div>
                            <?php if (get_option('2code_es_general_date_hidden', false) !== 'true'): ?>
                                <div class="col-xs-12 row-date"><?= $d->format($dateFormat); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>

            <!-- Mobile theme -->
            <div class="hidden-desktop">
                <?php for($i=0;$i<$dateCount;$i++): ?>
                    <?php

                    $current = $postArray[$i];
                    $day = $current['day'];
                    $locationCount = count($current['locations']);

                    ?>
                    <?php $date = str_replace('/', '-', get_field('event_day_date', $day->ID)); ?>
                    <?php $d = new DateTime($date) ?>
                    <div>
                        <div class="days-mobile">
                            <div class="col-xs-12 col-sm-12 scheduled-day mobile <?= $i === 0 ? 'active' : '' ?>" data-date="<?= $d->format('Y-m-d') ?>">
                                <div class="row">
                                    <div class="col-xs-12 row-day"><?= get_the_title($day->ID); ?></div>
                                    <?php if (get_option('2code_es_general_date_hidden', false) !== 'true'): ?>
                                        <div class="col-xs-12 row-date"><?= $d->format($dateFormat); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 scheduled-locations mobile locations-hidden" data-date="<?= $d->format('Y-m-d') ?>">
                            <?php for($j=0;$j<$locationCount;$j++): ?>
                                <?php $location = $current['locations'][$j] ?>
                                <div class="row scheduled-location <?= $j === 0 ? 'active' : '' ?>" data-location="<?= $location->slug ?>">
                                    <div class="col-xs-12"><?= $location->name ?></div>
                                </div>
                            <?php endfor; ?>
                        </div>
                        <div class="col-xs-12 scheduled-events hidden mobile">
                            <?php $k = 0; ?>
                            <?php foreach ($current['events'] as $event): ?>
                                <?php $post = $event['event']; ?>
                                <?php setup_postdata($post); ?>

                                <?php $time = new DateTime($event['time']); ?>
                                <?php $time = $time->format($timeFormat); ?>
                                <div class="row scheduled-event event-collapsed event-hidden event-<?=$d->format('Ymd')?>-<?= $k ?>" data-event="event-<?=$d->format('Ymd')?>-<?= $k ?>" data-date="<?= $d->format('Y-m-d') ?>" data-location="<?= $event['location'] ?>">
                                    <div class="col-xs-12">
                                        <div class="row">
                                            <?php $class = !has_post_thumbnail($post->ID) && get_option('2code_es_general_image_format', 'circle') !== 'none' ? 'hidden-xs' : ''; ?>
                                            <div class="col-sm-2 col-xs-12 vcenter <?= $class ?>">
                                                <?php if (get_option('2code_es_general_image_format', 'circle') !== 'none'): ?>
                                                    <div class="imgContainer center-block text-center">
                                                        <?php if (has_post_thumbnail($post->ID)): ?>
                                                            <?php $imgFormat = get_option('2code_es_general_image_format', 'circle') === 'circle' ? '2code-square-thumbnail' : '2code-rect-thumbnail'; ?>
                                                            <?php $url = get_the_post_thumbnail_url($post->ID, $imgFormat); ?>
                                                            <?php $class = stristr($url, '.svg') !== false ? 'svg' : ''; ?>
                                                            <img class="img-responsive center-block <?= $class ?>" src="<?= $url ?>" />
                                                        <?php endif; ?>
                                                    </div>
                                                <?php else: ?>
                                                    <div class="event-time"><?= $time ?></div>
                                                <?php endif; ?>
                                            </div><?php
                                            ?><div class="col-sm-9 col-xs-10 vcenter">
                                                <div class="row">
                                                    <?php if (get_option('2code_es_general_image_format', 'circle') !== 'none'): ?>
                                                        <div class="col-xs-12 event-time"><?= $time ?></div>
                                                    <?php endif; ?>
                                                    <div class="col-xs-12 event-title"><?= get_the_title($post->ID); ?></div>
                                                </div>
                                            </div><?php
                                            ?><div class="col-sm-1 col-xs-2 vcenter">
                                                <div class="event-icon">
                                                    <i class="tcode-ico ico-grot-down" data-state="collapsed"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 event-excerpt">
                                                <div class="row">
                                                    <div class="col-sm-10 col-sm-offset-2 col-xs-12">
                                                        <?php the_content(); ?>
                                                    </div>
                                                </div>
                                                <?php if ($artist = get_field('event_artist')): ?>
                                                    <?php $artist = reset($artist); ?>
                                                    <div class="row artist-row mobile">
                                                        <div class="col-xs-12 col-sm-10 col-sm-offset-2 vcenter">
                                                            <div class="vcenter artist-image artist-single-row">
                                                                <?php if (has_post_thumbnail($artist->ID)): ?>
                                                                    <?php $url = get_the_post_thumbnail_url($artist->ID, '2code-square-thumbnail'); ?>
                                                                    <img class="img-responsive" src="<?= $url ?>" />
                                                                <?php endif; ?>
                                                            </div>
                                                            <div class="vcenter artist-title-container artist-single-row">
                                                                <div class="artist-title">
                                                                    <?= get_the_title($artist->ID) ?>
                                                                </div>
                                                                <?php if ($position = get_field('artist_title', $artist->ID)): ?>
                                                                    <div class="artist-position">
                                                                        <?= $position ?>
                                                                    </div>
                                                                <?php endif; ?>
                                                            </div>
                                                            <?php $media = get_field('social_icons', $artist->ID) ?>
                                                            <?php if (!empty($media)): ?>
                                                                <div class="vcenter artist-social artist-single-row">
                                                                    <?php foreach ($media as $icon): ?>
                                                                        <a href="<?= $icon['social_network_url'] ?>" target="_blank" class="icon icon-<?= $icon['icon_type'] ?>"></a>
                                                                    <?php endforeach; ?>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $k++; ?>
                            <?php endforeach; ?>

                            <?php wp_reset_postdata(); ?>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
            <!-- End Mobile theme -->
        </div>

        <!-- Desktop theme -->
        <div class="hidden-mobile">
            <?php

            foreach($postArray as $day):
                $locationCount = count($day['locations']);
                $locationCols = apply_filters('2code-count-columns', $locationCount);

                $date = str_replace('/', '-', get_field('event_day_date', $day['day']->ID));
                $date = new DateTime($date);

                $mdCols = $locationCols;
                $smCols = $locationCols < $maxSM  ? $maxSM : $locationCols;
                $xsCols = $locationCols < $maxXSLocation ? $maxXSLocation : $locationCols;

                ?>
                <div class="row scheduled-locations desktop locations-hidden" data-date="<?= $date->format('Y-m-d') ?>">
                    <?php for($i=0;$i<$locationCount;$i++): ?>
                        <?php $location = $day['locations'][$i] ?>
                        <div class="col-md-<?= $mdCols ?> col-sm-12 col-xs-12 scheduled-location <?= $i === 0 ? 'active' : '' ?>" data-location="<?= $location->slug ?>">
                            <div class="row">
                                <div class="col-xs-12"><?= $location->name ?></div>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            <?php endforeach; ?>

            <div class="scheduled-events desktop">
                <?php $i = 0; ?>
                <?php foreach($postArray as $day): ?>

                    <?php $date = str_replace('/', '-', get_field('event_day_date', $day['day']->ID)); ?>
                    <?php $date = new DateTime($date); ?>

                    <?php foreach ($day['events'] as $event): ?>
                        <?php $post = $event['event']; ?>
                        <?php setup_postdata($post); ?>

                        <?php $time = new DateTime($event['time']); ?>
                        <?php $time = $time->format($timeFormat); ?>
                        <div class="row scheduled-event event-collapsed event-visible" data-date="<?= $date->format('Y-m-d') ?>" data-location="<?= $event['location'] ?>">
                            <div class="col-md-12">
                                <div class="row">
                                    <?php $class = !has_post_thumbnail($post->ID) && get_option('2code_es_general_image_format', 'circle') !== 'none' ? 'hidden-xs' : ''; ?>
                                    <div class="col-md-2 vcenter <?= $class ?>">
                                        <?php if (get_option('2code_es_general_image_format', 'circle') !== 'none'): ?>
                                            <div class="imgContainer center-block text-center">
                                                <?php if (has_post_thumbnail($post->ID)): ?>
                                                    <?php $imgFormat = get_option('2code_es_general_image_format', 'circle') === 'circle' ? '2code-square-thumbnail' : '2code-rect-thumbnail'; ?>
                                                    <?php $url = get_the_post_thumbnail_url($post->ID, $imgFormat); ?>
                                                    <?php $class = stristr($url, '.svg') !== false ? 'svg' : ''; ?>
                                                    <img class="img-responsive center-block <?= $class ?>" src="<?= $url ?>" />
                                                <?php endif; ?>
                                            </div>
                                        <?php else: ?>
                                            <div class="event-time text-center"><?= $time ?></div>
                                        <?php endif; ?>
                                    </div><div class="col-md-9 vcenter">
                                        <div class="row">
                                            <?php if (get_option('2code_es_general_image_format', 'circle') !== 'none'): ?>
                                                <div class="col-md-12 event-time"><?= $time ?></div>
                                            <?php endif; ?>
                                            <div class="col-md-12 event-title"><?= get_the_title($post->ID); ?></div>
                                        </div>
                                    </div><div class="col-md-1 vcenter">
                                        <div class="event-icon">
                                            <i class="tcode-ico ico-grot-down" data-state="collapsed"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 event-excerpt">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <?php the_content(); ?>
                                            </div>
                                        </div>
                                        <?php if ($artist = get_field('event_artist')): ?>
                                            <?php $artist = reset($artist); ?>
                                            <div class="row artist-row">
                                                <div class="col-md-10 col-md-offset-2 artist-title-col vcenter">
                                                    <div class="vcenter artist-image">
                                                        <?php if (has_post_thumbnail($artist->ID)): ?>
                                                            <?php $url = get_the_post_thumbnail_url($artist->ID, '2code-square-thumbnail'); ?>
                                                            <img class="img-responsive" src="<?= $url ?>" />
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="vcenter artist-title-container">
                                                        <div class="artist-title">
                                                            <?= get_the_title($artist->ID) ?>
                                                        </div>
                                                        <?php if ($position = get_field('artist_title', $artist->ID)): ?>
                                                            <div class="artist-position">
                                                                <?= $position ?>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                    <?php $media = get_field('social_icons', $artist->ID) ?>
                                                    <?php if (!empty($media)): ?>
                                                        <div class="vcenter">
                                                            <?php foreach ($media as $icon): ?>
                                                                <a href="<?= $icon['social_network_url'] ?>" target="_blank" class="icon icon-<?= $icon['icon_type'] ?>"></a>
                                                            <?php endforeach; ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                <?php endforeach; ?>

                <?php wp_reset_postdata(); ?>
                <div class="row no-events text-center">
                    <div class="col-xs-12">Select date to see events.</div>
                </div>
            </div>
        </div>
        <!-- End Desktop theme -->
    </div>
</div>

<?php $slides = $cnt < 4 ? $cnt : 4; ?>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        // Init schedule script
        $('.event-schedule').schedule({
            showLocations: <?= get_option('2code_es_general_locations_mode', 'true') ?>,
            keepAccordionsOpen: <?= get_option('2code_es_general_accordion_behavior', 'false') ?>,
            openFirstAccordion: <?= get_option('2code_es_general_accordion_open_first', 'false') ?>
        });
        $('#days-carousel').tcodeslick({
            slidesToShow: <?= $slides ?>,
            slidesToScroll: <?= $slides ?>,
            infinite: false,
            prevArrow: '<a href="#" class="slick-prev"></a>',
            nextArrow: '<a href="#" class="slick-next"></a>',
            speed: 800,
            easing: 'swing'
        });
    });
</script>
