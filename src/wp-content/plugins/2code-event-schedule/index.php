<?php

/*
Plugin Name: Event Scheduler
Plugin URI:
Description: Clean and easy to use event scheduler for WordPress. Organize time, place and main actors of your conference, training, festival or meeting.
Version: 1.1.1.5
Author: 2code_sc
Author URI: http://event-scheduler.pixelmint.net
*/

// Setup working directories
define('TCODE_ES_DIR', WP_PLUGIN_DIR . '/' . dirname(plugin_basename(__FILE__)));
define('TCODE_ES_URL', plugin_dir_url(__FILE__));

// Bootstrap file (loader)
include_once(TCODE_ES_DIR . '/functions.php');
// Init custom hooks and shortcodes
include_once(TCODE_ES_DIR . '/Api.php');

class EventSchedule {
    public function __construct() {
        // Initialize custom post types
        add_action('init', array($this, 'loadCustomTypes'));
        // Load styles
        add_action('wp_enqueue_scripts', array($this, 'loadStyles'));
        // Load scripts
        add_action('wp_enqueue_scripts', array($this, 'loadScripts'));
        // Load inline styles (needed to pass settings to CSS)
        add_action('wp_enqueue_scripts', function() {
            require TCODE_ES_DIR . '/inlineStyles.php';
        });

        // Initialize custom fields
        if (get_option('2code_es_general_use_builtin_acf', true) !== 'false') {
            add_action('acf/register_fields', array($this, 'loadCustomFields'));
        } else {
            add_action('acf/init', array($this, 'loadCustomFields'));
        }
    }

    public function loadStyles() {
        wp_enqueue_style('2code-base-style', TCODE_ES_URL . 'assets/css/basestyle.min.css');
        wp_enqueue_style('2code-slider-style', TCODE_ES_URL . 'assets/plugins/slick/slick.css');
        wp_enqueue_style('2code-schedule-style', TCODE_ES_URL . 'assets/css/style.css');
        wp_enqueue_style('2code-schedule-icons', TCODE_ES_URL . 'assets/css/social-icons.css');
    }

    public function loadScripts() {
        $settings = array(
            'imageType' => get_option('2code_es_general_image_format', 'circle')
        );

        wp_enqueue_script('2code-schedule-slider', TCODE_ES_URL . 'assets/plugins/slick/slick.min.js', array('jquery'), null, true);
        // Main script file
        wp_enqueue_script('2code-schedule-script', TCODE_ES_URL . 'assets/js/script.js', array('2code-schedule-slider', 'jquery-ui-core', 'jquery-ui-widget', 'jquery-effects-core'), null, true);
        // Pass settings to js file
        wp_localize_script('2code-schedule-script', 'settings', $settings);
    }

    public function loadCustomTypes() {
        $types = array('DayType', 'ArtistType', 'EventType');

        foreach ($types as $type) {
            $type::registerType();
        }
    }

    public function loadCustomFields() {
        $fields = array('DayFields', 'ArtistFields', 'EventFields');

        foreach ($fields as $field) {
            $field::loadFields();
        }
    }

}

// Autoinit
new EventSchedule();